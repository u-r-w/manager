
//import libray so we can use it in our programming journey
import React from 'react';  
import { Text, View } from 'react-native'; //import just library that we want to use it spesificaly

// this is component(something that dev we want to display to the screen) 
export const Header = (prop) => {   
    const { textStyle, viewStyle } = styles;  // destructing object from style object

    return (
        <View style={viewStyle}>
           <Text style={textStyle}>{prop.headerText}</Text>
        </View>       
    );
};


const styles = {
    viewStyle: {   // this is not CSS syntax
        backgroundColor: '#F8F8F8',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        paddingTop: 15,
        shadowOffset: { width: 10, height: 10, },
        shadowColor: 'black',
        shadowOpacity: 1.0,
        shadowRadius: 0.2,
        elevation: 2,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20   
    }
};
